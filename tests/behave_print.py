#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
from typing import Any


def bprint(obj: Any, is_dict: bool = False) -> None:
    print("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")
    if is_dict:
        print(json.dumps(obj, indent=4, sort_keys=True))
    else:
        print(obj)
    print("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")
    print()
