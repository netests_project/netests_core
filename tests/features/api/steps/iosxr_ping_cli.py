#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from netests_core.api import NetestsAPI
from netests_core.tools.base_tools import bprint


@given(u'[NetestsAPI] - Create a NetestsAPI object to execute ping')
def step_impl(context):
    context.api = NetestsAPI(
        username='admin',
        password='C1sco12345',
        host='sbx-iosxr-mgmt.cisco.com',
        port=8181,
        connexion_type='cli'
    )


@given(u'[NetestsAPI] - Execute iosxr_ping_cli() to {to} vrf {vrf} convert={convert} raw={raw}')  # noqa
def step_impl(context, to, vrf, convert, raw):
    convert = True if convert == 'true' else False
    raw = True if raw == 'true' else False

    context.data = context.api.iosxr_ping_cli(
        to=to, vrf=vrf, convert=convert, raw=raw
    )
    bprint(context.data)


@given(u'[NetestsAPI] - Function output.works is {works}')
def step_impl(context, works):
    works = True if works == 'true' else False

    if isinstance(context.data, dict):
        for i in context.data:
            if isinstance(i, dict):
                assert i.get('works') == works
