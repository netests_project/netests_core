#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from netests_core.api import NetestsAPI
from netests_core.tools.base_tools import bprint


@given(u'[NetestsAPI] - Create a NetestsAPI object to get VRF from a {device_type} named {host} with {connexion_type} on port {port} credentials=({username}, {password})')  # noqa
def step_impl(context, device_type, host, connexion_type, port, username, password):  # noqa
    context.api = NetestsAPI(
        username=username,
        password=password,
        host=host,
        port=int(port),
        connexion_type=connexion_type,
        device_type=device_type
    )


@given(u'[NetestsAPI] - Execute get_vrf() and use a converter')
def step_impl(context):
    output = context.api.get_vrf(convert=True)
    bprint(output.dict())
