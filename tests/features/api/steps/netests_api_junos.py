#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from netests_core.api import NetestsAPI
from netests_core.tools.base_tools import bprint


@given(u'[NetestsAPI](junos) - Create a NetestsAPI object to get {protocol} with {connexion_type} on port {port}')  # noqa
def step_impl(context, protocol, connexion_type, port):
    context.api = NetestsAPI(
        username='jcluser',
        password='Juniper!1',
        host='66.129.235.11',
        port=int(port),
        connexion_type=connexion_type
    )


@given(u'[NetestsAPI](junos) - Execute junos_get_vrf_cli() method with convert={convert} and raw={raw} output_format={output_format}')  # noqa
def step_impl(context, convert, raw, output_format):
    convert = True if convert == 'true' else False
    raw = True if raw == 'true' else False

    output = context.api.junos_get_vrf_cli(convert=convert, raw=raw)

    if raw is False and convert is True:
        bprint(output.dict())
    else:
        bprint(output, output=output_format)


@given(u'[NetestsAPI](junos) - Execute junos_get_vrf_netconf() method with convert={convert} and raw={raw} output_format={output_format}')  # noqa
def step_impl(context, convert, raw, output_format):
    convert = True if convert == 'true' else False
    raw = True if raw == 'true' else False

    output = context.api.junos_get_vrf_netconf(convert=convert, raw=raw)

    if convert is True:
        bprint(output.dict())
    else:
        bprint(output, output='str')


@given(u'[NetestsAPI](junos) - Execute junos_get_vrf_api() method with convert={convert} and raw={raw} output_format={output_format}')  # noqa
def step_impl(context, convert, raw, output_format):
    convert = True if convert == 'true' else False
    raw = True if raw == 'true' else False

    output = context.api.junos_get_vrf_api(convert=convert, raw=raw)

    if convert is True:
        bprint(output.dict())
    else:
        bprint(output, output='str')
