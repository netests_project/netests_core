#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from netests_core.api import NetestsAPI
from netests_core.tools.base_tools import bprint


@given(u'[NetestsAPI] - Create a NetestsAPI object to get {protocol} with {connexion_type} on port {port}')  # noqa
def step_impl(context, protocol, connexion_type, port):
    context.api = NetestsAPI(
        username='admin',
        password='C1sco12345',
        host='sbx-iosxr-mgmt.cisco.com',
        port=int(port),
        connexion_type=connexion_type
    )


@given(u'[NetestsAPI] - Execute iosxr_get_vrf() method with convert={convert} and raw={raw} output_format={output_format}')  # noqa
def step_impl(context, convert, raw, output_format):
    convert = True if convert == 'true' else False
    raw = True if raw == 'true' else False

    output = context.api.iosxr_get_vrf(convert=convert, raw=raw)

    if raw is False and convert is True:
        bprint(output.dict())
    else:
        bprint(output, output=output_format)
