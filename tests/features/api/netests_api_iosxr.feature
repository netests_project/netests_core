# Dylan Hamel - November 2020
Feature: Test class <NetestsAPI> defined in netests_core.api
    Scenario Outline: Create NetestsAPI - VRF object and try functions
        Given [NetestsAPI] - Create a NetestsAPI object to get <protocol> with <connexion_type> on port <port>
        And [NetestsAPI] - Execute iosxr_get_vrf() method with convert=<convert> and raw=<raw> output_format=<output_format>

    Examples: Types
        | convert | raw   | protocol | connexion_type | port  | output_format |
        | false   | false | vrf      | netconf        | 10000 | dict          |
        | true    | false | vrf      | netconf        | 10000 | dict          |
        | false   | true  | vrf      | netconf        | 10000 | dict          |
        | true    | true  | vrf      | netconf        | 10000 | dict          |
        
        | false   | false | vrf      | cli            | 8181  | str           |
        | true    | false | vrf      | cli            | 8181  | str           |
        | false   | true  | vrf      | cli            | 8181  | str           |
        | true    | true  | vrf      | cli            | 8181  | str           |