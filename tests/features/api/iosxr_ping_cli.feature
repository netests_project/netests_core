# Dylan Hamel - November 2020
Feature: Test function <NetestsAPI.iosxr_ping_cli()> defined in netests_core.api
    Scenario Outline: Create NetestsAPI and use iosxr_ping_cli() function
        Given [NetestsAPI] - Create a NetestsAPI object to execute ping
        And [NetestsAPI] - Execute iosxr_ping_cli() to <to> vrf <vrf> convert=<convert> raw=<raw>
        And [NetestsAPI] - Function output.works is <works> 

    Examples: Pings
        | to        | vrf     | convert | raw   | works |
        | 1.1.1.100 | default | false   | false | true  |
        | 1.1.1.100 | default | true    | false | true  |
        | 1.1.1.100 | default | false   | true  | true  |
        | 1.1.1.100 | default | true    | true  | true  |