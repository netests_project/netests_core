# Dylan Hamel - November 2020
Feature: Test function <NetestsAPI.get_vrf()> defined in netests_core.api
    Scenario Outline: Create NetestsAPI and use get_vrf() function
        Given [NetestsAPI] - Create a NetestsAPI object to get VRF from a <device_type> named <host> with <connexion_type> on port <port> credentials=(<username>, <password>)
        And [NetestsAPI] - Execute get_vrf() and use a converter

    Examples: Types
        | host                     | username | password   | connexion_type | port  | device_type |
        | sbx-iosxr-mgmt.cisco.com | admin    | C1sco12345 | netconf        | 10000 | iosxr       |
        | 66.129.235.11            | jcluser  | Juniper!1  | cli            | 44005 | junos       |
        | 66.129.235.11            | jcluser  | Juniper!1  | netconf        | 44003 | junos       |
        | 66.129.235.11            | jcluser  | Juniper!1  | api            | 44004 | junos       |