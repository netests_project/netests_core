# Dylan Hamel - November 2020
Feature: Test class <NetestsAPI> defined in netests_core.api
    @junos_cli
    Scenario Outline: Create NetestsAPI - VRF object and try functions
        Given [NetestsAPI](junos) - Create a NetestsAPI object to get <protocol> with <connexion_type> on port <port>
        And [NetestsAPI](junos) - Execute junos_get_vrf_cli() method with convert=<convert> and raw=<raw> output_format=<output_format>

    Examples: Types
        | convert | raw   | protocol | connexion_type | port  | output_format |
        | false   | false | vrf      | cli            | 44005 | dict          |
        | true    | false | vrf      | cli            | 44005 | dict          |
        | false   | true  | vrf      | cli            | 44005 | dict          |
        | true    | true  | vrf      | cli            | 44005 | dict          |


    @junos_nc
    Scenario Outline: Create NetestsAPI - VRF object and try functions
        Given [NetestsAPI](junos) - Create a NetestsAPI object to get <protocol> with <connexion_type> on port <port>
        And [NetestsAPI](junos) - Execute junos_get_vrf_netconf() method with convert=<convert> and raw=<raw> output_format=<output_format>

    Examples: Types
        | convert | raw   | protocol | connexion_type | port  | output_format |
        | false   | false | vrf      | netconf        | 44003 | dict          |
        | true    | false | vrf      | netconf        | 44003 | dict          |
        | false   | true  | vrf      | netconf        | 44003 | dict          |
        | true    | true  | vrf      | netconf        | 44003 | dict          |


    @junos_api
    Scenario Outline: Create NetestsAPI - VRF object and try functions
        Given [NetestsAPI](junos) - Create a NetestsAPI object to get <protocol> with <connexion_type> on port <port>
        And [NetestsAPI](junos) - Execute junos_get_vrf_api() method with convert=<convert> and raw=<raw> output_format=<output_format>

    Examples: Types
        | convert | raw   | protocol | connexion_type | port  | output_format |
        | false   | false | vrf      | api            | 44004 | dict          |
        | true    | false | vrf      | api            | 44004 | dict          |
        | false   | true  | vrf      | api            | 44004 | dict          |
        | true    | true  | vrf      | api            | 44004 | dict          |
