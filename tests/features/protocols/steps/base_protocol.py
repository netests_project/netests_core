#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from tests.behave_print import bprint
from netests_core.protocols.base_protocol import BaseProtocol


@given(u'[NetestsAPI] - Create a BaseProtocol object with_options={with_options}')  # noqa
def step_impl(context, with_options):
    with_options = True if with_options == 'true' else False

    if with_options:
        context.obj = BaseProtocol(
            options={
                'compare': {'options': True},
                'print': {'options': True}
            }
        )
    else:
        context.obj = BaseProtocol()


@given(u'[NetestsAPI] - Function __repr__ works')
def step_impl(context):
    bprint(context.obj, is_dict=False)
    s = context.obj.__repr__()
    assert 'options' not in s
    assert '{}' in s


@given(u'[NetestsAPI] - Function dict() has the correct keys ({keys})')
def step_impl(context, keys):
    bprint(context.obj.dict(), is_dict=True)
    s = context.obj.dict()
    assert 'options' in s.keys()
    assert len(s.keys()) == 1


@given(u'[NetestsAPI] - Function json() has the correct keys ({keys})')
def step_impl(context, keys):
    bprint(context.obj.json(), is_dict=True)
    s = context.obj.json()
    assert 'options' not in s.keys()
    assert len(s.keys()) == 0


@given(u'[NetestsAPI] - Function __eq__ and __ne__ works')
def step_impl(context):
    bprint(
        str(context.obj == context.obj) + str(context.obj != context.obj),
        is_dict=True
    )
    assert context.obj == context.obj
    assert not context.obj != context.obj
