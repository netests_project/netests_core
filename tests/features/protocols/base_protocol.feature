# Dylan Hamel - November 2020
Feature: Test class <NetestsAPI> defined in netests_core.api
    Scenario Outline: Create NetestsAPI - VRF object and try functions
        Given [NetestsAPI] - Create a BaseProtocol object with_options=<with_options>
        And [NetestsAPI] - Function __repr__ works
        And [NetestsAPI] - Function dict() has the correct keys (<keys_dict>)
        And [NetestsAPI] - Function json() has the correct keys (<keys_json>)
        And [NetestsAPI] - Function __eq__ and __ne__ works 


    Examples: Types
        | with_options | keys_dict   | keys_json |
        | false        | __NOT_SET__ | options   |
        | true         | __NOT_SET__ | options   |