#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from netests_core.tools.base_tools import bprint
from netests_core.devices.iosxr_nc import NetestsIOSXR


@given(u'[NetestsIOSXR] - Create a NetestsIOSXR object with commands={cmd_type}')  # noqa
def step_impl(context, cmd_type):
    if cmd_type == 'str':
        context.api = NetestsIOSXR(
            username='admin',
            password='C1sco12345',
            commands='<vrfs xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg"/>',  # noqa
            host='sbx-iosxr-mgmt.cisco.com',
            port=10000
        )
    elif cmd_type == 'dict':
        context.api = NetestsIOSXR(
            username='admin',
            password='C1sco12345',
            commands={
                'vrf': '<vrfs xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg"/>',  # noqa
                'bgp': '<bgp xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg"/>'  # noqa
            },
            host='sbx-iosxr-mgmt.cisco.com',
            port=10000
        )


@given(u'[NetestsIOSXR] - Execute execute() method')
def step_impl(context):
    context.data = context.api.execute()
    bprint(context.data)


@then(u'[NetestsIOSXR] - Return object is {data_type}')
def step_impl(context, data_type):
    if data_type == 'dict':
        assert isinstance(context.data, dict)
    else:
        assert False


@then(u'[NetestsIOSXR] - Return object contains {nb_key} key(s)')
def step_impl(context, nb_key):
    assert len(context.data) == int(nb_key)


@then(u'[NetestsIOSXR] - Return object key(s) is/are {key_name}')
def step_impl(context, key_name):
    key_name = key_name.split(',')
    for i in key_name:
        assert i in context.data
