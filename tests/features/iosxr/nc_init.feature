# Dylan Hamel - November 2020
Feature: Test class <NetestsIOSXR> defined in netests_core.devices.iosxr_nc
    Scenario Outline: Create NetestsIOSXR object and try functions
        Given [NetestsIOSXR] - Create a NetestsIOSXR object with commands=<cmd_type>
        And [NetestsIOSXR] - Execute execute() method

        Then [NetestsIOSXR] - Return object is <data_type>
        And [NetestsIOSXR] - Return object contains <nb_key> key(s)
        And [NetestsIOSXR] - Return object key(s) is/are <key_name> 

    Examples: Types
        | cmd_type | data_type | nb_key | key_name |
        | str      | dict      | 1      | cmd      |
        | dict     | dict      | 2      | vrf,bgp  |