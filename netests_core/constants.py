#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os


NOT_SET = '__not_set__'
PATH = os.path.dirname(__file__)
TEMPLATES_PATH = f"{PATH}/templates/"
TEXTFSM_PATH = f"{TEMPLATES_PATH}textfsm/"


API = 'api'
CLI = 'cli'
NETCONF = 'netconf'
RESTCONF = 'restconf'
CONNEXION_TYPES = [API, CLI, NETCONF, RESTCONF]

ARISTA = 'eos'
CUMULUS = 'linux'
EXTREME_VSP = 'extreme_vsp'
CISCO_IOS = 'ios'
CISCO_IOSXR = 'iosxr'
JUNOS = 'junos'
CISCO_NXOS = 'nxos'
NAPALM = 'napalm'
DEVICE_TYPES = [
    ARISTA, CUMULUS, CISCO_IOS, CISCO_IOSXR, CISCO_NXOS,
    EXTREME_VSP, JUNOS, NAPALM, NOT_SET
]

COMPARE_OPT_KEY = 'compare'
PRINT_OPT_KEY = 'print'


BGP = 'bgp'
CDP = 'cdp'
LLDP = 'lldp'
DISCOVERY_PROTOCOLS = [CDP, LLDP]
FACTS = 'facts'
ISIS = 'isis'
OSPF = 'ospf'
PING = 'ping'
SOCKET = 'socket'
VRF = 'vrf'
