#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from scrapli.driver.core import IOSXRDriver
from typing import Optional, Union, Dict, Any
from netests_core.devices.base_device_cli import BaseDeviceCLI


class NetestsIOSXRCLI(BaseDeviceCLI):
    def __init__(
        self,
        username: str,
        password: str,
        commands: Union[Dict[str, str], str],
        host: str,
        port: Optional[int] = 22,
        timeout: Optional[int] = 30,
        hostkey_verify: Optional[bool] = False,
    ):
        super().__init__(
            username=username,
            password=password,
            commands=commands,
            host=host,
            port=port,
            timeout=timeout,
            hostkey_verify=hostkey_verify,
        )

    def exec_cmd(self, command: str, connexion: IOSXRDriver) -> str:
        return connexion.send_command(command)

    def execute(self) -> Dict[Any, Any]:
        with IOSXRDriver(
            host=self.host,
            port=self.port,
            auth_username=self.username,
            auth_password=self.password,
            auth_strict_key=self.hostkey_verify,
        ) as c:
            if isinstance(self.commands, str):
                data = self.exec_cmd(command=self.commands, connexion=c)
                self.store(data.result, command=self.commands)
            elif isinstance(self.commands, dict):
                for vrf, values in self.commands.items():
                    self.raw_data[vrf] = dict()
                    self.data[vrf] = dict()
                    for key, command in values.items():
                        data = self.exec_cmd(command=command, connexion=c)
                        self.store(data.result, command=key, vrf=vrf)

            return self.data
