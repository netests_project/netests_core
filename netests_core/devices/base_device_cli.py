#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import Optional, Union, Dict
from netests_core.constants import NOT_SET
from netests_core.devices.base_device import BaseDevice


class BaseDeviceCLI(BaseDevice):
    def __init__(
        self,
        username: str,
        password: str,
        commands: Union[Dict[str, str], str],
        host: str,
        port: Optional[int] = 22,
        timeout: Optional[int] = 30,
        hostkey_verify: Optional[bool] = False,
    ):
        super().__init__(
            username=username,
            password=password,
            commands=commands,
            host=host,
            port=port
        )
        self.timeout: int = timeout
        self.hostkey_verify: bool = hostkey_verify

    def store(self, data: str, command: str, vrf: str = NOT_SET) -> None:
        if vrf == NOT_SET:
            self.raw_data[command] = data
            self.data[command] = data
        else:
            self.raw_data[vrf][command] = data
            self.data[vrf][command] = data
