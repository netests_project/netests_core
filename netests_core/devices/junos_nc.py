#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from lxml import etree
from jnpr.junos import Device
from typing import Optional, Union, Dict, Any
from netests_core.devices.base_device import BaseDevice


class NetestsJunosNetconf(BaseDevice):
    def __init__(
        self,
        username: str,
        password: str,
        commands: Union[Dict[str, str], str],
        host: str,
        port: Optional[int] = 22,
        timeout: Optional[int] = 30,
        hostkey_verify: Optional[bool] = False,
    ):
        super().__init__(
            username=username,
            password=password,
            commands=commands,
            host=host,
            port=port
        )
        self.timeout: int = timeout
        self.hostkey_verify: bool = hostkey_verify

    def exec_cmd(self, command: str, connexion: Device) -> Optional[str]:
        if command == 'get_instance_information':
            data = connexion.rpc.get_instance_information(detail=True)
        else:
            return None

        return etree.tostring(data)

    def execute(self) -> Dict[Any, Any]:
        with Device(
            host=self.host,
            port=self.port,
            user=self.username,
            password=self.password
        ) as c:
            if isinstance(self.commands, str):
                data = self.exec_cmd(command=self.commands, connexion=c)
                self.store(data, command=self.commands)
            elif isinstance(self.commands, dict):
                for vrf, values in self.commands.items():
                    self.raw_data[vrf] = dict()
                    self.data[vrf] = dict()
                    for key, command in values.items():
                        data = self.exec_cmd(command=command, connexion=c)
                        self.store(data, command=key, vrf=vrf)

            return self.data
