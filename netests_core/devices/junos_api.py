#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
from typing import Optional, Union, Dict, Any
from netests_core.devices.base_device_api import BaseDeviceAPI


class NetestsJunosAPI(BaseDeviceAPI):
    def __init__(
        self,
        username: str,
        password: str,
        commands: Union[Dict[str, str], str],
        host: str,
        port: Optional[int] = 22,
        timeout: Optional[int] = 30,
        hostkey_verify: Optional[bool] = False,
    ):
        super().__init__(
            username=username,
            password=password,
            commands=commands,
            host=host,
            port=port,
            timeout=timeout,
            hostkey_verify=hostkey_verify
        )

    def exec_cmd(self, command: str) -> Optional[str]:
        protocol = self.use_https(self.hostkey_verify)

        res = requests.get(
            url=f"{protocol}://{self.host}:{self.port}/rpc/{command}",
            headers={'content-type': 'application/xml'},
            auth=requests.auth.HTTPBasicAuth(
                f"{self.username}",
                f"{self.password}"
            ),
            verify=self.hostkey_verify
        )
        self.check_status_code(res.status_code)
        return res.content

    def execute(self) -> Dict[Any, Any]:
        if isinstance(self.commands, str):
            data = self.exec_cmd(command=self.commands)
            self.store(data, command=self.commands)
        elif isinstance(self.commands, dict):
            for vrf, values in self.commands.items():
                self.raw_data[vrf] = dict()
                self.data[vrf] = dict()
                for key, command in values.items():
                    data = self.exec_cmd(command=command)
                    self.store(data, command=key, vrf=vrf)

        return self.data
