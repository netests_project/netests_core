#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from abc import ABC, abstractmethod
from typing import Optional, Union, Dict, List, Any
from netests_core.constants import NOT_SET
from netests_core.tools.base_tools import bprint
from netests_core.tools.base_tools import validate_xml as vxml
from netests_core.tools.base_tools import format_xml as fxml


class BaseDevice(ABC):
    def __init__(
        self,
        username: str,
        password: str,
        commands: List[str],
        host: str,
        port: Optional[int] = 22
    ):
        self.username: str = username
        self.password: str = password
        self.commands: List[str] = commands
        self.host: str = host
        self.port: int = port
        self.data: Any = {}
        self.raw_data: Any = {}

    def __repr__(self) -> str:
        return str(self.dict())

    @abstractmethod
    def execute(self) -> Union[Dict[Any, Any], List[Dict[Any, Any]]]:
        pass

    def validate_xml(self, obj: Any) -> Any:
        return vxml(obj=obj)

    def format_xml(
        self,
        obj: Any,
        format: Optional[str] = 'xml'
    ) -> Dict[Any, Any]:
        return fxml(obj=obj, format=format)

    def store(self, data: str, command: str, vrf: str = NOT_SET) -> None:
        if vrf == NOT_SET:
            self.raw_data[command] = data
            self.data[command] = data
        else:
            self.raw_data[vrf][command] = data
            self.data[vrf][command] = data

    def print(self) -> None:
        bprint(self.dict())

    def jprint(self, obj: Dict[Any, Any]) -> None:
        bprint(obj)
