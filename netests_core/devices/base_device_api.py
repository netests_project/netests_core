#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import Optional, Union, Dict
from netests_core.devices.base_device import BaseDevice
from netests_core.exceptions import NetestsHTTPStatusCodeError


class BaseDeviceAPI(BaseDevice):
    def __init__(
        self,
        username: str,
        password: str,
        commands: Union[Dict[str, str], str],
        host: str,
        port: Optional[int] = 22,
        timeout: Optional[int] = 30,
        hostkey_verify: Optional[bool] = False,
    ):
        super().__init__(
            username=username,
            password=password,
            commands=commands,
            host=host,
            port=port
        )
        self.timeout: int = timeout
        self.hostkey_verify: bool = hostkey_verify

    def check_status_code(self, status_code):
        if status_code != 200:
            raise NetestsHTTPStatusCodeError()

    def use_https(self, secure_api: str) -> str:
        return "https" if secure_api else "http"
