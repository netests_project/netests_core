#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from ncclient import manager
from ncclient.manager import Manager
from typing import Optional, Union, Dict, Any
from netests_core.devices.base_device import BaseDevice


class NetestsIOSXRNetconf(BaseDevice):
    def __init__(
        self,
        username: str,
        password: str,
        commands: Union[Dict[str, str], str],
        host: str,
        port: Optional[int] = 22,
        hostkey_verify: Optional[bool] = False,
        timeout: Optional[int] = 30,
        source: Optional[str] = 'running',
        method: Optional[str] = 'get_config'
    ):
        super().__init__(
            username=username,
            password=password,
            commands=commands,
            host=host,
            port=port
        )
        self.hostkey_verify: bool = hostkey_verify
        self.timeout: int = timeout
        self.source: str = source
        self.data: Any = {}
        self.raw_data: Any = {}

    def converter(self):
        pass

    def get_config(self, command: str, manager: Manager) -> str:
        data = manager.get_config(
            source=self.source,
            filter=('subtree', (command))
        ).data_xml
        self.validate_xml(data)
        return data

    def execute(self) -> Dict[Any, Any]:
        with manager.connect(
            host=self.host,
            port=self.port,
            username=self.username,
            password=self.password,
            hostkey_verify=self.hostkey_verify,
            device_params={'name': 'iosxr'}
        ) as m:
            if isinstance(self.commands, str):
                data = self.get_config(command=self.commands, manager=m)
                self.raw_data['cmd'] = data
                self.data['cmd'] = self.format_xml(data)
            elif isinstance(self.commands, dict):
                for key, command in self.commands.items():
                    data = self.get_config(command=command, manager=m)
                    self.raw_data[key] = data
                    self.data[key] = self.format_xml(data)

            return self.data
