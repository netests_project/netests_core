#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from netests_core.constants import (
    CISCO_IOSXR, JUNOS,
    VRF,
    API, CLI, NETCONF
)


C = {
    CISCO_IOSXR: {
        CLI: {
            VRF: "show vrf all detail"
        }
    },
    JUNOS: {
        API: {
            VRF: "get-instance-information?detail="
        },
        CLI: {
            VRF: "show route instance detail | display json"
        },
        NETCONF: {
            VRF: "get_instance_information"
        }
    }
}
