#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from netests_core.constants import NOT_SET
from netests_core.tools.base_tools import format_xml
from netests_core.protocols.vrf_protocol import NetestsVRF
from netests_core.converters.base_converter import BaseConverter
from netests_core.protocols.base_protocol import BaseListProtocol


class IOSXRVRFNCConverter(BaseConverter):
    def lets_go(self) -> BaseListProtocol:
        if self.cmd_output.get('VRF') is not None:
            self.cmd_output['VRF'] = format_xml(self.cmd_output.get('VRF'))
        if self.cmd_output.get('BGP') is not None:
            self.cmd_output['BGP'] = format_xml(self.cmd_output.get('BGP'))

        vrf_list = BaseListProtocol(elements=list())

        vrf_list.elements.append(
            NetestsVRF(
                vrf_name="default",
                vrf_id=NOT_SET,
                vrf_type=NOT_SET,
                l3_vni=NOT_SET,
                rd=NOT_SET,
                rt_imp=NOT_SET,
                rt_exp=NOT_SET,
                imp_targ=NOT_SET,
                exp_targ=NOT_SET,
                options=self.options
            )
        )

        if (
            'VRF' in self.cmd_output.keys() and
            'data' in self.cmd_output.get('VRF').keys() and
            'vrfs' in self.cmd_output.get('VRF').get('data').keys() and
            'vrf' in self.cmd_output.get('VRF').get('data').get('vrfs').keys()
        ):
            if isinstance(
                self.cmd_output.get('VRF').get('data').get('vrfs').get('vrf'),
                dict
            ):
                rias = None
                riin = None
                reas = None
                rein = None
                v = self.cmd_output.get('VRF') \
                                   .get('data') \
                                   .get('vrfs') \
                                   .get('vrf')
                if (
                        'afs' in v.keys() and
                        'af' in v.get('afs').keys() and
                        'bgp' in v.get('afs').get('af').keys()
                ):
                    rias = v.get('afs').get('af').get('bgp') \
                                                 .get('import-route-targets') \
                                                 .get('route-targets') \
                                                 .get('route-target') \
                                                 .get('as-or-four-byte-as') \
                                                 .get('as')

                    riin = v.get('afs').get('af').get('bgp') \
                                                 .get('import-route-targets') \
                                                 .get('route-targets') \
                                                 .get('route-target') \
                                                 .get('as-or-four-byte-as') \
                                                 .get('as-index')

                    reas = v.get('afs').get('af').get('bgp') \
                                                 .get('export-route-targets') \
                                                 .get('route-targets') \
                                                 .get('route-target') \
                                                 .get('as-or-four-byte-as') \
                                                 .get('as')

                    rein = v.get('afs').get('af').get('bgp') \
                                                 .get('export-route-targets') \
                                                 .get('route-targets') \
                                                 .get('route-target') \
                                                 .get('as-or-four-byte-as') \
                                                 .get('as-index')

                rd = NOT_SET
                if (
                    self.cmd_output.get('BGP') is not None and
                    'data' in self.cmd_output.get('BGP').keys() and
                    'bgp' in self.cmd_output.get('BGP')
                                    .get('data').keys() and
                    'instance' in self.cmd_output.get('BGP')
                                            .get('data')
                                            .get('bgp').keys() and
                    'instance-as' in self.cmd_output.get('BGP')
                                            .get('data')
                                            .get('bgp')
                                            .get('instance').keys() and
                    'four-byte-as' in self.cmd_output.get('BGP')
                                                .get('data')
                                                .get('bgp')
                                                .get('instance')
                                                .get('instance-as').keys() and
                    'vrfs' in self.cmd_output.get('BGP')
                                             .get('data')
                                             .get('bgp')
                                             .get('instance')
                                             .get('instance-as')
                                             .get('four-byte-as').keys() and
                    'vrf' in self.cmd_output.get('BGP')
                                    .get('data')
                                    .get('bgp')
                                    .get('instance')
                                    .get('instance-as')
                                    .get('four-byte-as')
                                    .get('vrfs').keys()
                ):
                    if isinstance(self.cmd_output.get('BGP')
                                                 .get('data')
                                                 .get('bgp')
                                                 .get('instance')
                                                 .get('instance-as')
                                                 .get('four-byte-as')
                                                 .get('vrfs')
                                                 .get('vrf'), dict):
                        vrf = self.cmd_output.get('BGP') \
                                        .get('data') \
                                        .get('bgp') \
                                        .get('instance') \
                                        .get('instance-as') \
                                        .get('four-byte-as') \
                                        .get('vrfs') \
                                        .get('vrf')
                        if vrf.get('vrf-name') == v.get('vrf-name'):
                            if (
                                'vrf-global' in vrf.keys() and
                                'route-distinguisher' in vrf.get('vrf-global')
                                                                .keys()
                            ):
                                rd = vrf.get('vrf-global') \
                                        .get('route-distinguisher') \
                                        .get('as') + ":" + \
                                    vrf.get('vrf-global') \
                                        .get('route-distinguisher') \
                                        .get('as-index')

                    elif isinstance(self.cmd_output.get('BGP')
                                                   .get('data')
                                                   .get('bgp')
                                                   .get('instance')
                                                   .get('instance-as')
                                                   .get('four-byte-as')
                                                   .get('vrfs')
                                                   .get('vrf'), list):

                        for vrf in self.cmd_output.get('BGP') \
                                            .get('data') \
                                            .get('bgp') \
                                            .get('instance') \
                                            .get('instance-as') \
                                            .get('four-byte-as') \
                                            .get('vrfs') \
                                            .get('vrf'):
                            if vrf.get('vrf-name') == v.get('vrf-name'):
                                if (
                                    'vrf-global' in vrf.keys() and
                                    'route-distinguisher' in
                                    vrf.get('vrf-global').keys()
                                ):
                                    rd = vrf.get('vrf-global') \
                                            .get('route-distinguisher') \
                                            .get('as') + ":" + \
                                        vrf.get('vrf-global') \
                                            .get('route-distinguisher') \
                                            .get('as-index')
                vrf_list.elements.append(
                    NetestsVRF(
                        vrf_name=v.get('vrf-name'),
                        vrf_id=NOT_SET,
                        vrf_type=NOT_SET,
                        l3_vni=NOT_SET,
                        rd=rd,
                        rt_imp=f"{rias}:{riin}"
                            if rias is not None else NOT_SET,
                        rt_exp=f"{reas}:{rein}"
                            if reas is not None else NOT_SET,
                        imp_targ=NOT_SET,
                        exp_targ=NOT_SET,
                        options=self.options
                    )
                )

            elif isinstance(
                self.cmd_output.get('VRF').get('data').get('vrfs').get('vrf'),
                list
            ):
                for v in self.cmd_output.get('VRF') \
                                        .get('data') \
                                        .get('vrfs') \
                                        .get('vrf'):
                    rias = None
                    riin = None
                    reas = None
                    rein = None
                    if (
                        'afs' in v.keys() and 'af' in v.get('afs').keys() and
                        'bgp' in v.get('afs').get('af').keys()
                    ):
                        rias = v.get('afs') \
                                .get('af') \
                                .get('bgp') \
                                .get('import-route-targets') \
                                .get('route-targets') \
                                .get('route-target') \
                                .get('as-or-four-byte-as') \
                                .get('as')

                        riin = v.get('afs') \
                                .get('af') \
                                .get('bgp') \
                                .get('import-route-targets') \
                                .get('route-targets') \
                                .get('route-target') \
                                .get('as-or-four-byte-as') \
                                .get('as-index')

                        reas = v.get('afs') \
                                .get('af') \
                                .get('bgp') \
                                .get('export-route-targets') \
                                .get('route-targets') \
                                .get('route-target') \
                                .get('as-or-four-byte-as') \
                                .get('as')

                        rein = v.get('afs') \
                                .get('af') \
                                .get('bgp') \
                                .get('export-route-targets') \
                                .get('route-targets') \
                                .get('route-target') \
                                .get('as-or-four-byte-as') \
                                .get('as-index')

                    rd = NOT_SET
                    if (
                        self.cmd_output.get('BGP') is not None and
                        'data' in self.cmd_output.get('BGP').keys() and
                        'bgp' in self.cmd_output.get('BGP')
                                        .get('data').keys() and
                        'instance' in self.cmd_output.get('BGP')
                                                .get('data')
                                                .get('bgp').keys() and
                        'instance-as' in self.cmd_output.get('BGP')
                                                .get('data')
                                                .get('bgp')
                                                .get('instance').keys() and
                        'four-byte-as' in self.cmd_output.get('BGP')
                                                .get('data')
                                                .get('bgp')
                                                .get('instance')
                                                .get('instance-as').keys() and
                        'vrfs' in self.cmd_output.get('BGP')
                                            .get('data')
                                            .get('bgp')
                                            .get('instance')
                                            .get('instance-as')
                                            .get('four-byte-as').keys() and
                        'vrf' in self.cmd_output.get('BGP')
                                            .get('data')
                                            .get('bgp')
                                            .get('instance')
                                            .get('instance-as')
                                            .get('four-byte-as')
                                            .get('vrfs').keys()
                    ):
                        if isinstance(self.cmd_output.get('BGP')
                                                     .get('data')
                                                     .get('bgp')
                                                     .get('instance')
                                                     .get('instance-as')
                                                     .get('four-byte-as')
                                                     .get('vrfs')
                                                     .get('vrf'), dict):
                            vrf = self.cmd_output.get('BGP') \
                                            .get('data') \
                                            .get('bgp') \
                                            .get('instance') \
                                            .get('instance-as') \
                                            .get('four-byte-as') \
                                            .get('vrfs') \
                                            .get('vrf')
                            if vrf.get('vrf-name') == v.get('vrf-name'):
                                if (
                                    'vrf-global' in vrf.keys() and
                                    'route-distinguisher' in vrf.get('vrf-global').keys()  # noqa
                                ):
                                    rd = vrf.get('vrf-global') \
                                            .get('route-distinguisher') \
                                            .get('as') + ":" + \
                                        vrf.get('vrf-global') \
                                           .get('route-distinguisher') \
                                           .get('as-index')

                        elif isinstance(self.cmd_output.get('BGP')
                                                       .get('data')
                                                       .get('bgp')
                                                       .get('instance')
                                                       .get('instance-as')
                                                       .get('four-byte-as')
                                                       .get('vrfs')
                                                       .get('vrf'), list):

                            for vrf in self.cmd_output.get('BGP') \
                                                .get('data') \
                                                .get('bgp') \
                                                .get('instance') \
                                                .get('instance-as') \
                                                .get('four-byte-as') \
                                                .get('vrfs') \
                                                .get('vrf'):
                                if vrf.get('vrf-name') == v.get('vrf-name'):
                                    if (
                                        'vrf-global' in vrf.keys() and
                                        'route-distinguisher' in
                                        vrf.get('vrf-global').keys()
                                    ):
                                        rd = vrf.get('vrf-global') \
                                                .get('route-distinguisher') \
                                                .get('as') + ":" + \
                                            vrf.get('vrf-global') \
                                                .get('route-distinguisher') \
                                                .get('as-index')

                    vrf_list.elements.append(
                        NetestsVRF(
                            vrf_name=v.get('vrf-name'),
                            vrf_id=NOT_SET,
                            vrf_type=NOT_SET,
                            l3_vni=NOT_SET,
                            rd=rd,
                            rt_imp=f"{rias}:{riin}"
                                        if rias is not None else NOT_SET,
                            rt_exp=f"{reas}:{rein}"
                                        if reas is not None else NOT_SET,
                            imp_targ=NOT_SET,
                            exp_targ=NOT_SET,
                            options=self.options
                        )
                    )

        return vrf_list
