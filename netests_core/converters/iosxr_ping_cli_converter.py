#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from netests_core.tools.base_tools import parse_textfsm
from netests_core.protocols.ping_protocol import NetestsPing
from netests_core.converters.base_converter import BaseConverter
from netests_core.protocols.base_protocol import BaseListProtocol


class IOSXRPingCLIConverter(BaseConverter):
    def lets_go(self) -> BaseListProtocol:
        if isinstance(self.cmd_output, dict):
            for key in self.cmd_output.keys():
                self.cmd_output = self.cmd_output.get(key)

        self.cmd_output = parse_textfsm(
            content=self.cmd_output,
            template_file='cisco_xr_ping.textfsm'
        )

        list_ping = BaseListProtocol(elements=list())

        for ping in self.cmd_output:
            list_ping.elements.append(
                NetestsPing(
                    to=ping[2],
                    count=ping[0],
                    size=ping[1],
                    timeout=ping[3],
                    success_rate=ping[4],
                    success_count=ping[5],
                    rt_min=ping[6],
                    rt_avg=ping[7],
                    rt_max=ping[8]
                )
            )

        return list_ping
