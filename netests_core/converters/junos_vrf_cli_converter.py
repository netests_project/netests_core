#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
from netests_core.constants import NOT_SET
from netests_core.protocols.vrf_protocol import NetestsVRF
from netests_core.converters.junos_converter import JunosConverter
from netests_core.protocols.base_protocol import BaseListProtocol


class JunosVRFCLIConverter(JunosConverter):
    def lets_go(self) -> BaseListProtocol:
        if not isinstance(self.cmd_output.get(self.command), dict):
            cmd_output = json.loads(self.cmd_output.get(self.command))
        else:
            cmd_output = self.cmd_output.get(self.command)

        vrf_list = BaseListProtocol(elements=list())

        for vrf in cmd_output.get('instance-information')[0] \
                             .get('instance-core'):

            if self.vrf_filter(
                    vrf.get('instance-name')[0].get('data', NOT_SET)
            ):
                vrf_obj = NetestsVRF()

                vrf_obj.vrf_name = self.vrf_default_mapping(
                    vrf.get('instance-name')[0].get('data', NOT_SET)
                )
                vrf_obj.vrf_id = vrf.get('router-id')[0] \
                                    .get('data', NOT_SET)
                vrf_obj.vrf_type = vrf.get('instance-type')[0] \
                                      .get('data', NOT_SET)
                vrf_obj.l3_vni = NOT_SET

                if "instance-vrf" in vrf.keys():
                    vrf_obj.rd = vrf.get('instance-vrf')[0] \
                                    .get('route-distinguisher')[0] \
                                    .get('data', NOT_SET)
                    vrf_obj.rt_imp = vrf.get('instance-vrf')[0] \
                                        .get('vrf-import')[0] \
                                        .get('data', NOT_SET)
                    vrf_obj.rt_exp = vrf.get('instance-vrf')[0]\
                                        .get('vrf-export')[0] \
                                        .get('data', NOT_SET)
                    vrf_obj.imp_targ = vrf.get('instance-vrf')[0] \
                                          .get('vrf-import-target')[0] \
                                          .get('data', NOT_SET)
                    vrf_obj.exp_targ = vrf.get('instance-vrf')[0] \
                                          .get('vrf-export-target')[0] \
                                          .get('data', NOT_SET)

                vrf_obj.options = self.options
                vrf_list.elements.append(vrf_obj)

        return vrf_list
