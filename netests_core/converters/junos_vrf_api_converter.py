#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from netests_core.constants import NOT_SET
from netests_core.tools.base_tools import format_xml
from netests_core.protocols.vrf_protocol import NetestsVRF
from netests_core.converters.junos_converter import JunosConverter
from netests_core.protocols.base_protocol import BaseListProtocol


class JunosVRFAPIConverter(JunosConverter):
    def lets_go(self) -> BaseListProtocol:
        if not isinstance(self.cmd_output.get(self.command), dict):
            cmd_output = format_xml(self.cmd_output.get(self.command))
        else:
            cmd_output = self.cmd_output.get(self.command)

        vrf_list = BaseListProtocol(elements=list())

        for vrf in cmd_output.get('instance-information') \
                             .get('instance-core'):
            if self.vrf_filter(vrf.get('instance-name')):
                rd = NOT_SET
                rt_imp = NOT_SET
                rt_exp = NOT_SET
                imp_targ = NOT_SET
                exp_targ = NOT_SET
                if "instance-vrf" in vrf.keys():
                    rd = vrf.get('instance-vrf') \
                            .get('route-distinguisher', NOT_SET)
                    rt_imp = vrf.get('instance-vrf') \
                                .get('vrf-import', NOT_SET)
                    rt_exp = vrf.get('instance-vrf') \
                                .get('vrf-export', NOT_SET)
                    imp_targ = vrf.get('instance-vrf') \
                                  .get('vrf-import-target', NOT_SET)
                    exp_targ = vrf.get('instance-vrf') \
                                  .get('vrf-export-target', NOT_SET)

                vrf_list.elements.append(
                    NetestsVRF(
                        vrf_name=self.vrf_default_mapping(
                            vrf.get('instance-name')
                        ),
                        vrf_id=vrf.get('router-id', NOT_SET),
                        vrf_type=vrf.get('instance-type', NOT_SET),
                        l3_vni=NOT_SET,
                        rd=rd,
                        rt_imp=rt_imp,
                        rt_exp=rt_exp,
                        imp_targ=imp_targ,
                        exp_targ=exp_targ,
                        options=self.options
                    )
                )

        return vrf_list
