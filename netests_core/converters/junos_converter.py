#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from abc import ABC
from netests_core.converters.base_converter import BaseConverter


class JunosConverter(BaseConverter, ABC):
    def vrf_filter(self, vrf_name: str) -> bool:
        """This function will remove Juniper system VRF
            - "master"
            - "__juniper_private1__"
            - "__juniper_private2__"
            - "__juniper_private4__"
            - "__master.anon__"

        Args:
            vrf_name (str): [VRF name to check if it's a System VRF]

        Returns:
            bool: [Return True is it is not a VRF system]
        """
        return "__" not in vrf_name

    def vrf_default_mapping(self, vrf_name: str) -> str:
        """This function converts Junos global/default/master routing instance
            - master => default

        Args:
            vrf_name (str): [VRF name to convert]

        Returns:
            str: [VRF name after mapping]
        """
        if vrf_name == "master":
            return "default"
        else:
            return vrf_name
