#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from netests_core.constants import NOT_SET
from netests_core.tools.base_tools import parse_textfsm
from netests_core.protocols.vrf_protocol import NetestsVRF
from netests_core.converters.base_converter import BaseConverter
from netests_core.protocols.base_protocol import BaseListProtocol


class IOSXRVRFCLIConverter(BaseConverter):
    def lets_go(self) -> BaseListProtocol:
        self.cmd_output = self.cmd_output.get('default_vrf').get('no_key')
        self.cmd_output = re.sub(
            pattern=r"communities:[\n\r]\s+RT",
            repl="communities:RT",
            string=self.cmd_output
        )

        self.cmd_output = parse_textfsm(
            content=self.cmd_output,
            template_file='cisco_xr_show_vrf_all_detail.textfsm'
        )

        list_vrf = BaseListProtocol(elements=list())

        list_vrf.elements.append(
            NetestsVRF(
                vrf_name="default",
                vrf_id=NOT_SET,
                l3_vni=NOT_SET,
                rt_imp=NOT_SET,
                rt_exp=NOT_SET,
                imp_targ=NOT_SET,
                exp_targ=NOT_SET,
                options=self.options
            )
        )

        for nei in self.cmd_output:
            list_vrf.elements.append(
                NetestsVRF(
                    vrf_name=nei[0]
                        if nei[0] != "not set" and nei[0] != '' else NOT_SET,
                    vrf_id=NOT_SET,
                    vrf_type=nei[3]
                        if nei[3] != "not set" and nei[3] != '' else NOT_SET,
                    l3_vni=NOT_SET,
                    rd=nei[1]
                        if nei[1] != "not set" and nei[1] != '' else NOT_SET,
                    rt_imp=nei[5]
                        if nei[5] != "not set" and nei[5] != '' else NOT_SET,
                    rt_exp=nei[6]
                        if nei[6] != "not set" and nei[6] != '' else NOT_SET,
                    imp_targ=NOT_SET,
                    exp_targ=NOT_SET,
                    options=self.options
                )
            )

        return list_vrf
