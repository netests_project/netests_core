#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pydantic import BaseModel
from abc import ABC, abstractmethod
from typing import Optional, Dict, Any
from netests_core.protocols.base_protocol import BaseListProtocol


class BaseConverter(BaseModel, ABC):
    hostname: str
    cmd_output: Dict[str, Any]
    command: Optional[str]
    options: Optional[Dict[str, Any]] = {}

    @abstractmethod
    def lets_go(self) -> BaseListProtocol:
        pass
