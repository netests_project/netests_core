#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import lxml
import textfsm
import xmltodict
from xml.etree import ElementTree
from typing import Optional, Dict, Any
from netests_core.constants import TEXTFSM_PATH


def bprint(obj: Any, output='dict') -> None:
    print("\n")
    print("*" * 80)
    if output == 'dict':
        if isinstance(obj, dict):
            print(json.dumps(obj, indent=4, sort_keys=True))
        else:
            print(json.dumps(obj.dict(), indent=4, sort_keys=True))
    elif output == 'str':
        print(obj)
    print("*" * 80)
    print("\n")


def validate_xml(obj: Any) -> Any:
    if isinstance(obj, dict):
        return obj
    return ElementTree.fromstring(obj)


def format_xml(obj: Any, format: Optional[str] = 'xml') -> Dict[Any, Any]:
    if obj is None:
        return {}
    elif isinstance(obj, dict):
        return obj
    elif isinstance(obj, lxml.etree._Element):
        obj = json.dumps(
            xmltodict.parse(
                ElementTree.tostring(obj)
            )
        )
    elif isinstance(obj, str):
        obj = json.dumps(xmltodict.parse(obj))
    elif isinstance(obj, bytes):
        obj = json.dumps(xmltodict.parse(str(obj, 'utf-8')))
    return json.loads(obj)


def parse_textfsm(content, template_file) -> list:
    template = open(f"{TEXTFSM_PATH}{template_file}")
    results_template = textfsm.TextFSM(template)
    return results_template.ParseText(content)
