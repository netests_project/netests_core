#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import Optional, Union, Dict, List, Any
from netests_core.commands import C
from netests_core.devices.iosxr_cli import NetestsIOSXRCLI
from netests_core.devices.iosxr_nc import NetestsIOSXRNetconf
from netests_core.devices.junos_api import NetestsJunosAPI
from netests_core.devices.junos_cli import NetestsJunosCLI
from netests_core.devices.junos_nc import NetestsJunosNetconf
from netests_core.exceptions import (
    NetestsErrorConnexionType, NetestsErrorDeviceTypes
)
from netests_core.constants import (
    NOT_SET, DEVICE_TYPES, CONNEXION_TYPES,
    CISCO_IOSXR, JUNOS,
    API, CLI, NETCONF,
    VRF
)
from netests_core.converters.iosxr_ping_cli_converter import IOSXRPingCLIConverter  # noqa
from netests_core.converters.iosxr_vrf_cli_converter import IOSXRVRFCLIConverter  # noqa
from netests_core.converters.iosxr_vrf_nc_converter import IOSXRVRFNCConverter
from netests_core.converters.junos_vrf_api_converter import JunosVRFAPIConverter  # noqa
from netests_core.converters.junos_vrf_cli_converter import JunosVRFCLIConverter  # noqa
from netests_core.converters.junos_vrf_nc_converter import JunosVRFNCConverter  # noqa


CONVERTER_MAPPING = {
    "iosxr_ping_cli": IOSXRPingCLIConverter,
    "iosxr_vrf_cli": IOSXRVRFCLIConverter,
    "iosxr_vrf_nc": IOSXRVRFNCConverter,
    "junos_vrf_api": JunosVRFAPIConverter,
    "junos_vrf_cli": JunosVRFCLIConverter,
    "junos_vrf_nc": JunosVRFNCConverter
}


class NetestsAPI(object):
    def __init__(
        self,
        username: str,
        password: str,
        host: str,
        port: Optional[int] = 22,
        connexion_type: str = 'netconf',
        device_type: str = NOT_SET,
        commands: Optional[Union[List[str], Dict[str, str]]] = [],
        options: Optional[Dict[str, str]] = {}
    ):
        self.username: str = username
        self.password: str = password
        self.host: str = host
        self.port: int = port
        if connexion_type not in CONNEXION_TYPES:
            raise NetestsErrorConnexionType(
                f"Unsupported connextion_type. Use {CONNEXION_TYPES}"
                f" - <{connexion_type}> is given"
            )
        self.connexion_type: str = connexion_type
        if device_type not in DEVICE_TYPES:
            raise NetestsErrorDeviceTypes(
                f"Unsupported device_type. Use {DEVICE_TYPES}"
                f" - <{device_type}> is given"
            )
        self.device_type: str = device_type
        self.commands: Union[List[str], Dict[str, str]] = commands
        self.bind: Any = None
        self.options: Dict[str, str] = options

        self.FUNCTIONS_MAPPING = {
            "ping": {
                CISCO_IOSXR: {
                    CLI: self.iosxr_ping_cli
                }
            },
            "vrf": {
                CISCO_IOSXR: {
                    CLI: self.iosxr_get_vrf_cli,
                    NETCONF: self.iosxr_get_vrf_netconf
                },
                JUNOS: {
                    API: self.junos_get_vrf_api,
                    CLI: self.junos_get_vrf_cli,
                    NETCONF: self.junos_get_vrf_netconf,
                }
            }
        }

    def execute(
        self,
        function: str = '',
        command: str = '',
        convert: Optional[bool] = True,
        raw: Optional[bool] = False
    ) -> Union[Dict[str, Any], List[Any]]:
        self.bind.execute()
        if convert:
            converter = CONVERTER_MAPPING.get(function)(
                hostname=self.host,
                cmd_output=self.bind.data,
                command=command,
                options=self.options
            )
            return converter.lets_go()
        elif raw:
            return self.bind.raw_data
        else:
            return self.bind.data

    ###########################################################################
    # Initiators
    ###########################################################################
    def init_iosxr_nc(self, commands: str) -> None:
        self.bind = NetestsIOSXRNetconf(
            username=self.username,
            password=self.password,
            host=self.host,
            port=self.port,
            commands=commands
        )

    def init_iosxr_cli(self, commands: str) -> None:
        self.bind = NetestsIOSXRCLI(
            username=self.username,
            password=self.password,
            host=self.host,
            port=self.port,
            commands=commands
        )

    def init_junos_api(self, commands: str) -> None:
        self.bind = NetestsJunosAPI(
            username=self.username,
            password=self.password,
            host=self.host,
            port=self.port,
            commands=commands
        )

    def init_junos_cli(self, commands: str) -> None:
        self.bind = NetestsJunosCLI(
            username=self.username,
            password=self.password,
            host=self.host,
            port=self.port,
            commands=commands
        )

    def init_junos_nc(self, commands: str) -> None:
        self.bind = NetestsJunosNetconf(
            username=self.username,
            password=self.password,
            host=self.host,
            port=self.port,
            commands=commands
        )

    ###########################################################################
    # VRF endpoint
    ###########################################################################
    def get_vrf(
        self,
        convert: Optional[bool] = True,
        raw: Optional[bool] = False
    ) -> Dict[Any, Any]:
        if self.device_type != NOT_SET:
            return self.FUNCTIONS_MAPPING.get('vrf') \
                                         .get(self.device_type) \
                                         .get(self.connexion_type)(
                                             convert=convert, raw=raw
                                         )
        else:
            return "Impossible use this function, device_type is not set"

    def iosxr_get_vrf(
        self,
        convert: Optional[bool] = True,
        raw: Optional[bool] = False
    ) -> Dict[Any, Any]:
        if self.connexion_type == 'api':
            raise NotImplementedError()
        elif self.connexion_type == 'cli':
            return self.iosxr_get_vrf_cli(convert=convert, raw=raw)
        elif self.connexion_type == 'netconf':
            return self.iosxr_get_vrf_netconf(convert=convert, raw=raw)
        else:
            raise NotImplementedError()

    def iosxr_get_vrf_cli(
        self,
        convert: Optional[bool] = True,
        raw: Optional[bool] = False
    ):
        self.init_iosxr_cli(commands=C.get(CISCO_IOSXR).get(CLI).get(VRF))
        return self.execute(
            function='iosxr_vrf_cli',
            command=C.get(CISCO_IOSXR).get(CLI).get(VRF),
            convert=convert,
            raw=raw
        )

    def iosxr_get_vrf_netconf(
        self,
        convert: Optional[bool] = True,
        raw: Optional[bool] = False
    ):
        self.init_iosxr_nc(
            commands={
                'vrf': '<vrfs xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg"/>',  # noqa
                'bgp': '<bgp xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg"/>'  # noqa
            }
        )
        return self.execute(function='iosxr_vrf_nc', convert=convert, raw=raw)

    def junos_get_vrf(
        self,
        convert: Optional[bool] = True,
        raw: Optional[bool] = False
    ) -> Dict[Any, Any]:
        if self.connexion_type == 'api':
            return self.junos_get_vrf_api(convert=convert, raw=raw)
        elif self.connexion_type == 'cli':
            return self.junos_get_vrf_cli(convert=convert, raw=raw)
        elif self.connexion_type == 'netconf':
            return self.junos_get_vrf_netconf(convert=convert, raw=raw)
        else:
            raise NotImplementedError()

    def junos_get_vrf_api(
        self,
        convert: Optional[bool] = True,
        raw: Optional[bool] = False
    ):
        self.init_junos_api(commands=C.get(JUNOS).get(API).get(VRF))
        return self.execute(
            function='junos_vrf_api',
            command=C.get(JUNOS).get(API).get(VRF),
            convert=convert,
            raw=raw
        )

    def junos_get_vrf_cli(
        self,
        convert: Optional[bool] = True,
        raw: Optional[bool] = False
    ):
        self.init_junos_cli(
            commands=C.get(JUNOS).get(CLI).get(VRF)
        )
        return self.execute(
            function='junos_vrf_cli',
            command=C.get(JUNOS).get(CLI).get(VRF),
            convert=convert,
            raw=raw
        )

    def junos_get_vrf_netconf(
        self,
        convert: Optional[bool] = True,
        raw: Optional[bool] = False
    ):
        self.init_junos_nc(commands=C.get(JUNOS).get(NETCONF).get(VRF))
        return self.execute(
            function='junos_vrf_nc',
            command=C.get(JUNOS).get(NETCONF).get(VRF),
            convert=convert,
            raw=raw
        )

    ###########################################################################
    # Ping endpoint
    ###########################################################################
    def ping(
        self,
        to: str,
        vrf: str = 'default',
        count: int = 5,
        timeout: int = 2,
        convert: Optional[bool] = True,
        raw: Optional[bool] = False
    ) -> bool:
        if self.device_type != NOT_SET:
            return self.FUNCTIONS_MAPPING.get('ping') \
                                         .get(self.device_type) \
                                         .get(self.connexion_type)(
                                             to=to,
                                             vrf=vrf,
                                             count=count,
                                             timeout=timeout,
                                             convert=convert,
                                             raw=raw
                                         )
        else:
            return "Impossible use this function, device_type is not set"

    def iosxr_ping_cli(
        self,
        to: str,
        vrf: str = 'default',
        count: int = 5,
        timeout: int = 2,
        convert: Optional[bool] = True,
        raw: Optional[bool] = False
    ) -> bool:
        self.init_iosxr_cli(
            commands=f"ping {to} timeout {timeout} count {count} vrf {vrf}"
        )
        return self.execute(
            function='iosxr_ping_cli', convert=convert, raw=raw
        )
