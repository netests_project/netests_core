#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

from typing import Optional, Dict, List
from netests_core.protocols.base_protocol import BaseProtocol
from netests_core.constants import (
    NOT_SET, COMPARE_OPT_KEY, PRINT_OPT_KEY
)


class NetestsBGPSession(BaseProtocol):
    src_hostname: str = NOT_SET
    peer_ip: str = NOT_SET
    peer_hostname: str = NOT_SET
    remote_as: str = NOT_SET
    state_brief: str = NOT_SET
    session_state: str = NOT_SET
    state_time: str = NOT_SET
    prefix_received: str = NOT_SET
    options: Optional[Dict[str, Dict[str, bool]]] = {
        COMPARE_OPT_KEY: {
            'src_hostname': True,
            'peer_ip': True,
            'peer_hostname': False,
            'remote_as': True,
            'state_brief': True,
            'session_state': False,
            'state_time': False,
            'prefix_received': False
        },
        PRINT_OPT_KEY: {
            'src_hostname': True,
            'peer_ip': True,
            'peer_hostname': True,
            'remote_as': True,
            'state_brief': True,
            'session_state': True,
            'state_time': True,
            'prefix_received': True,
        }
    }


class NetestsBGPSessionsVRF(BaseProtocol):
    vrf_name: str = NOT_SET
    as_number: int = NOT_SET
    router_id: str = NOT_SET
    bgp_sessions: List[NetestsBGPSession] = []
    options: Optional[Dict[str, Dict[str, bool]]] = {
        COMPARE_OPT_KEY: {
            'vrf_name': True,
            'as_number': True,
            'router_id': True,
            'bgp_sessions': True
        },
        PRINT_OPT_KEY: {
            'vrf_name': True,
            'as_number': True,
            'router_id': True,
            'bgp_sessions': True
        },
    }
