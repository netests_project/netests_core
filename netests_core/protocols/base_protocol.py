#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pydantic import BaseModel
from typing import Optional, Union, Dict, List, Any
from netests_core.tools.base_tools import bprint
from netests_core.constants import PRINT_OPT_KEY, COMPARE_OPT_KEY


class BaseProtocol(BaseModel):
    options: Optional[Dict[str, Dict[str, bool]]] = {
        'compare': {'options': True},
        'print': {'options': True}
    }

    def format(self, format: str = 'dict') -> Union[Dict[str, Any], str]:
        result = dict()
        if PRINT_OPT_KEY in self.__dict__.get('options').keys():
            for k, v in self.__dict__.get('options') \
                                     .get(PRINT_OPT_KEY) \
                                     .items():
                if k != 'options' and v is True:
                    result[k] = self.__dict__.get(k)
        else:
            result = self.dict()
        if format == 'str':
            return str(result)
        else:
            return result

    def __repr__(self) -> str:
        return self.format(format='str')

    def json(self) -> str:
        return self.format(format='dict')

    def __str__(self) -> str:
        return self.format(format='str')

    def __ne__(self, o: object) -> bool:
        return not self.__eq__(o)

    def __eq__(self, o: object) -> bool:
        if type(self) != type(o):
            return NotImplementedError(
                f"Impossible to compare {type(self)} with type(o)"
            )
        if COMPARE_OPT_KEY in self.__dict__.get('options').keys():
            for k, v in self.__dict__.get('options') \
                                     .get(COMPARE_OPT_KEY) \
                                     .items():
                if k == 'options':
                    pass
                if v and (self.__dict__.get(k) != o.__dict__.get(k)):
                    return False
        return True

    def print(self):
        bprint(self.dict())


class BaseListProtocol(BaseProtocol):
    elements: List[Any] = []
