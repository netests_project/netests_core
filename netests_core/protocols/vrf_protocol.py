#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Optional, Dict
from netests_core.protocols.base_protocol import BaseProtocol
from netests_core.constants import NOT_SET, COMPARE_OPT_KEY, PRINT_OPT_KEY


class NetestsVRF(BaseProtocol):
    vrf_name: str = NOT_SET
    vrf_id: str = NOT_SET
    vrf_type: str = NOT_SET
    l3_vni: str = NOT_SET
    rd: str = NOT_SET
    rt_imp: str = NOT_SET
    rt_exp: str = NOT_SET
    imp_targ: str = NOT_SET
    exp_targ: str = NOT_SET
    options: Optional[Dict[str, Dict[str, bool]]] = {
        COMPARE_OPT_KEY: {
            'vrf_name': False,
            'vrf_id': True,
            'vrf_type': False,
            'l3_vni': False,
            'rd': False,
            'rt_imp': False,
            'rt_exp': False,
            'imp_targ': False,
            'exp_targ': False,
        },
        PRINT_OPT_KEY: {
            'vrf_name': True,
            'vrf_id': True,
            'vrf_type': True,
            'l3_vni': True,
            'rd': True,
            'rt_imp': True,
            'rt_exp': True,
            'imp_targ': True,
            'exp_targ': True,
        }
    }
