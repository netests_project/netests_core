#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Optional, Dict
from netests_core.constants import NOT_SET
from netests_core.protocols.base_protocol import BaseProtocol


class NetestsPing(BaseProtocol):
    to: str = NOT_SET
    count: int = -1
    size: int = -1
    timeout: int = -1
    success_rate: int = -1
    success_count: int = -1
    rt_min: int = -1
    rt_avg: int = -1
    rt_max: int = -1
    pourcent_works: float = (count / success_count)
    works: bool = (pourcent_works < 2)
    options: Optional[Dict[str, Dict[str, bool]]] = {}
