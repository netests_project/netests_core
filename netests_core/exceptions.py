#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class NetestsErrorConnexionType(Exception):
    pass


class NetestsErrorDeviceTypes(Exception):
    pass


class NetestsHTTPStatusCodeError(Exception):
    pass
