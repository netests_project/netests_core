#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from netests_core.api import NetestsAPI


api = NetestsAPI(
    username='admin',
    password='C1sco12345',
    host='sbx-iosxr-mgmt.cisco.com',
    port=8181,
    connexion_type='cli',
)
api.iosxr_ping_cli(to='1.1.1.100', count=1)

# Specify device_type - use generic ping function
api = NetestsAPI(
    username='admin',
    password='C1sco12345',
    host='sbx-iosxr-mgmt.cisco.com',
    port=8181,
    connexion_type='cli',
    device_type='iosxr'
)
api.ping(to='1.1.1.100', count=1)
