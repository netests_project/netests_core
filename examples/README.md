# Netests_core - Examples

How to use `netests_core` ?

## 01 - Basic with data formatted - Netconf 

```shell
>>> from netests_core import NetestsAPI
>>> api = NetestsAPI(
...     username='admin',
...     password='C1sco12345',
...     host='sbx-iosxr-mgmt.cisco.com',
...     port=10000,
...     connexion_type='netconf'
... )
>>>
>>> data = api.iosxr_get_vrf(convert=True)
>>>
>>> type(data)
<class 'netests_core.protocols.base_protocol.BaseListProtocol'>
>>>
>>> type(data.elements)
<class 'list'>
>>>
>>> print(data)
{'options': {}, 'elements': [{'options': {}, 'vrf_name': 'default', 'vrf_id': '__not_set__', 'vrf_typ': '__not_set__', 'l3_vni': '__not_set__', 'rd': '__not_set__', 'rt_imp': '__not_set__', 'rt_exp': '__not_set__', 'imp_targ': '__not_set__', 'exp_targ': '__not_set__'}]}
```



## 02 - Basic with data formatted  - CLI

```shell
>>> from netests_core import NetestsAPI
>>>
>>> api = NetestsAPI(
...     username='admin',
...     password='C1sco12345',
...     host='sbx-iosxr-mgmt.cisco.com',
...     port=8181,
...     connexion_type='cli'
... )
>>>
>>> api.iosxr_get_vrf(convert=True)
{'options': {}, 'elements': [{'options': {}, 'vrf_name': 'default', 'vrf_id': '__not_set__', 'vrf_typ': '__not_set__', 'l3_vni': '__not_set__', 'rd': '__not_set__', 'rt_imp': '__not_set__', 'rt_exp': '__not_set__', 'imp_targ': '__not_set__', 'exp_targ': '__not_set__'}]}
>>>
```



## 03 - Basic with raw data - Netconf

```shell
>>> from netests_core import NetestsAPI
>>>
>>> api = NetestsAPI(
...     username='admin',
...     password='C1sco12345',
...     host='sbx-iosxr-mgmt.cisco.com',
...     port=10000,
...     connexion_type='netconf'
... )
>>> data = api.iosxr_get_vrf(raw=True)
>>> print(data)
{'vrf': '<?xml version="1.0" encoding="UTF-8"?><data xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0"/>\n', 'bgp': '<?xml version="1.0" encoding="UTF-8"?><data xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0"/>\n'}
>>>
```



## 04 - Basic with raw data - CLI

```shell
>>> from netests_core import NetestsAPI
>>>
>>> api = NetestsAPI(
...     username='admin',
...     password='C1sco12345',
...     host='sbx-iosxr-mgmt.cisco.com',
...     port=8181,
...     connexion_type='cli'
... )
>>>
>>> data = api.iosxr_get_vrf(raw=True)
>>> print(data)
{'default_vrf': {'no_key': 'Sat Nov 21 23:33:15.526 UTC'}}
>>>
```



### 05 - Generic Ping - CLI

```shell
>>> from netests_core.api import NetestsAPI
>>> api = NetestsAPI(
...     username='admin',
...     password='C1sco12345',
...     host='sbx-iosxr-mgmt.cisco.com',
...     port=8181,
...     connexion_type='cli',
...     device_type='iosxr'
... )
>>> api.ping(to='1.1.1.100', count=1)
{
	'options': {},
	'elements': [
		{
			'options': {},
			'to': '1.1.1.100',
			'count': 1,
			'size': 100,
			'timeout': 2,
			'success_rate': 100,
			'success_count': 1,
			'rt_min': 1,
			'rt_avg': 1,
			'rt_max': 1,
			'works': True
		}
	]
}
```

