#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from netests_core import NetestsAPI

api = NetestsAPI(
    username='admin',
    password='C1sco12345',
    host='sbx-iosxr-mgmt.cisco.com',
    port=10000,
    connexion_type='netconf'
)

data = api.iosxr_get_vrf(convert=True)
